import { environment } from '../environments/environment';
export const  apiUrl = {
    home        :  "http://testing.birdapps.org/launderette-v3/cms/api-web-v3/home/home",
    getInTouch  : environment.baseApiUrl+ 'contact_us_web/contact_us_email',
    trending    : environment.baseApiUrl+ 'home/trending' ,
    guideList   : environment.baseApiUrl+ 'Guide_web/guideList',
    guideDetails: environment.baseApiUrl+ 'Guide_web/guideDetails'
}