import { Routes } from '@angular/router';

import { GuideInformationComponent } from './guide-information.component'
export const routes: Routes = [
    { path: '' , component: GuideInformationComponent , pathMatch:'full'}
]