import { Meta , Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from  '@angular/router';
import { HttpCallService } from '../../helper/httpCall.service'
import { LoadScript } from './../../helper/loadScript';
import { apiUrl } from '../../app.constants';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-guide-information',
  templateUrl: './guide-information.component.html',
  styleUrls: ['./guide-information.component.css']
})
export class GuideInformationComponent implements OnInit {

  loadAPI                  : Promise<any>;
  name                     : String;
  guideDetail              : Object;
  guidePackageDetail       : Object;
  OtherPackages            : Array<Object>;
  readTypeHandle           : String = "Read More";
  readTypeHandleForIntinery: String = "Read More";
  expectedLength           : number = 300;
  expectedLengthForItinery : number = 300;
  packageBanner            : Array<String>;
  baseImageUrl             : String;
  metaName: String;
  constructor(private httpCall: HttpCallService , private route: ActivatedRoute , private meta: Meta , private title: Title ) {
    this.route.params.subscribe((params) => {
      this.metaName = params.name.charAt(0).toUpperCase()+(params.name.replace(/-/g," ")).slice(1);
    });
    this.title.setTitle(this.metaName+" Tour Guides - Odigos");
    this.meta.addTags([{ name:"description" , content: 'I am '+ this.metaName +' working as a private tour guide. If you want to know about our culture, Art & history of the tourist place then Hire me & Enjoy your trip hassle-free. I love to stay in touch with people.' } ,
    {name:'keywords' , content: 'Hire tour guides, India tour Guide, best tour guide India'}]);
  }

  ngOnInit() {
    this.baseImageUrl = environment.baseImageUrl;
    this.route.params.subscribe((params) => {
      this.name = params.name;
    });
    this.httpCall.callApi( 'POST' , apiUrl.guideDetails , {web_url: this.name , place_url:''}).subscribe(async (res) => {
      if(res && res["body"] && res["body"].status==1) {
        await this.updatingValueOnComponentInit(res);
        this.loadScript();
      }
    });
  }
  updatingValueOnComponentInit(res){
    this.guideDetail        = res["body"].guideDetails;
    this.OtherPackages      = res["body"].guideDetails.packages;
    this.OtherPackages.filter((res) => { 
      res['guide_charges'] = Math.round(res['guide_charges']);
    });
    this.guidePackageDetail = res["body"].packageDetails;
    this.packageBanner      = res["body"].packageBanners;
  }
  loadScript(){ 
    this.loadAPI = new Promise((resolve) => {
      new LoadScript().loadScript(["assets/js/custom.js","assets/js/slick.min.js"]);
      resolve(true);
    });
  }
  readMoreClick(type , checkType){
    if(checkType == 'about_me'){
      switch(this.readTypeHandle){ 
        case "Read More":{
          this.readTypeHandle = "...Read Less";
          this.expectedLength = type.length;
          break;
        }
        case "...Read Less":{
          this.readTypeHandle = "Read More";
          this.expectedLength = 300;
          break;
        }
      }
    }else{ 
      switch(this.readTypeHandleForIntinery){ 
        case "Read More":{
          this.readTypeHandleForIntinery = "...Read Less";
          this.expectedLengthForItinery  = type.length;
          break;
        }
        case "...Read Less":{
          this.readTypeHandleForIntinery = "Read More";
          this.expectedLengthForItinery  = 300;
          break;
        }
      }
    }
  }
  
}
