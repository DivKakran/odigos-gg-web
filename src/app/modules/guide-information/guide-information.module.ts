import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { routes } from './guide-information.routing';
import { RouterModule } from '@angular/router';
import { GuideInformationComponent } from './guide-information.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes) ,
  ],
  declarations: [GuideInformationComponent],
  exports: [ GuideInformationComponent ]
})
export class GuideInformationModule { }
