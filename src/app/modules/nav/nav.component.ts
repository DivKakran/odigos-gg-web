import { Component, OnInit} from '@angular/core';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  baseImageUrl: String;
  isIphone: boolean  = false;
  isAndroid: boolean = false;

  isMobile = {
    iOS: function() {
      return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Android: function() {
      return navigator.userAgent.match(/Android/i);
    },
  }

  constructor() { }

  ngOnInit() {
    this.baseImageUrl = environment.baseImageUrl;
    if(this.isMobile.iOS()) { 
      this.isIphone = true;
    }else if(this.isMobile.Android()){
      this.isAndroid = true;
    }
  }
  closeDownloadMenu(){
    let c = document.querySelector('.appLink');
    document.getElementById("appLink").style.display = "none";
  }
  clickMenu(){
    let body = document.getElementsByTagName('body')[0];
    body.classList.add("backdrop");   //add the class
    var element = document.createElement("div");
    element.setAttribute("class", 'overlay');
    document.body.appendChild(element);
  }
  removeMenuClass(){
    let c = document.querySelector('.overlay');
    c.classList.remove('overlay');
    let backdrop = document.querySelector('.backdrop');
    backdrop.classList.remove('backdrop');
    // let body = document.getElementsByTagName('body')[0];
    // body.classList.remove("overlay"); 
  }

}
