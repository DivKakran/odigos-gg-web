export class Validations{
    public getInTouchValidate(value){ 
        let _temp = { isOkay : false , msg:'' }
        if(value.contact_name == ""){
            _temp.msg = "Name is required";
            return _temp;
        }
        else if(value.contact_email == ""){
            _temp.msg = "Email is required";
            return _temp;
        }else if(!(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(String(value.contact_email).toLowerCase()))){
            _temp.msg = "Email is not valid";
            return _temp;
        }else if(value.contact_mobile == ""){
            _temp.msg = "Phone is required";
            return _temp;
        }else if(value.contact_mobile.length < 10){
            _temp.msg = "Phone isn't valid";
            return _temp;
        }else if(value.contact_city == ""){
            _temp.msg = "City is required";
            return _temp;
        }else if(value.contact_comments == ""){ 
            _temp.msg = "Comment is required";
            return _temp;
        } else{
            _temp.isOkay = true;
            return _temp;
        }
    }
}