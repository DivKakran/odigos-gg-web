import { Subject } from 'rxjs';
import { Injectable } from "@angular/core";

@Injectable({'providedIn':'root'})
export class DataPassingService {
    guideDetail = new Subject();
    constructor(){}
    sendGuideDetail(data){
        this.guideDetail.next(data.name);
    }
}