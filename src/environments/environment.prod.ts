export const environment = {
  production: true,
  baseImageUrl: 'assets/images/',
  baseApiUrl: 'http://prod.birdapps.org/guide-v2/cms/api-v4/'
};
